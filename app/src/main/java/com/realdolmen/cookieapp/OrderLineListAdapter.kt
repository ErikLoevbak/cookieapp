package com.realdolmen.cookieapp
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.realdolmen.cookieapp.activities.OrderDetails
import com.realdolmen.cookieapp.domain.OrderLine

class OrderLineListAdapter(private var list: ArrayList<OrderLine>, private val context : Context) :
    RecyclerView.Adapter<OrderLineListAdapter.ViewHolder>()  {

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun bindItem(orderLine : OrderLine) {
            var product = itemView.findViewById(R.id.product_name)as TextView
            var amount = itemView.findViewById(R.id.product_amount)as TextView
            var price = itemView.findViewById(R.id.total_price)as TextView
            product.text = orderLine.product!!.sName
            amount.text = orderLine.amount.toString()
            price.text = "$" + orderLine.getFullPrice().toString() + "0"

        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): OrderLineListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cardview_orderline, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: OrderLineListAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }
}