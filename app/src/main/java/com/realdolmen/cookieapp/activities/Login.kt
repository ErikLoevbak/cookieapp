package com.realdolmen.cookieapp.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.realdolmen.cookieapp.R
import kotlinx.android.synthetic.main.login.*




class Login : AppCompatActivity() {

    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        goto_register.setOnClickListener { register() }
        login.setOnClickListener { login() }
        auth = FirebaseAuth.getInstance()

    }

    public override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(currentUser: FirebaseUser?) {
        if(currentUser != null){
            val intent = Intent(this, CentralHub::class.java)
            startActivity(intent)
        } else{
            return
        }
    }

    private var back_pressed: Long = 0
    private var toast: Toast? = null
    override fun onBackPressed() {


        if (back_pressed + 2000 > System.currentTimeMillis()) {

            // need to cancel the toast here
            toast!!.cancel()

            // code for exit
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)

        } else {
            // ask user to press back button one more time to close app
            toast = Toast.makeText(baseContext, "Press BACK again to exit", Toast.LENGTH_SHORT)
            toast!!.show()
        }
        back_pressed = System.currentTimeMillis()
    }

    fun login(){
        if(!login_email.text.isBlank() && !login_password.text.isBlank()) {
            auth.signInWithEmailAndPassword(login_email.text.toString(), login_password.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = auth.currentUser
                        Toast.makeText(baseContext, "Welcome " + user!!.displayName + "!", Toast.LENGTH_LONG).show()
                        updateUI(user)
                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(
                            baseContext, "Incorrect username or password.",
                            Toast.LENGTH_SHORT
                        ).show()
                        updateUI(null)
                    }
                }
        } else {
            Toast.makeText(baseContext, "Incorrect username or password.", Toast.LENGTH_SHORT).show()
        }
    }

    fun register(){
        val intent = Intent(this, Register::class.java)
        startActivity(intent)
    }
}