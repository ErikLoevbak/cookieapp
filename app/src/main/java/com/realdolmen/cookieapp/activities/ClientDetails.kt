package com.realdolmen.cookieapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.cookieapp.R
import com.realdolmen.cookieapp.domain.Client
import com.realdolmen.cookieapp.domain.Member
import com.realdolmen.cookieapp.domain.Order
import com.realdolmen.cookieapp.domain.OrderLine
import kotlinx.android.synthetic.main.client_details.*

class ClientDetails : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.client_details)



        auth = FirebaseAuth.getInstance()
    }

    fun placeOrder(view: View) {
        val db = FirebaseFirestore.getInstance()
        var order: Order = intent.getSerializableExtra("order") as Order
        val client = Client()
        client.firstName = et_firstname.text.toString()
        client.lastName = et_lastname.text.toString()
        client.street = et_street.text.toString()
        client.houseNumber = et_number.text.toString()
        client.postalCode = et_postalcode.text.toString()
        client.phone = et_phone.text.toString()
        client.city = et_city.text.toString()
        if(client.firstName == "" ||
                client.lastName == "" ||
                client.street == "" ||
                client.houseNumber == "" ||
                client.postalCode == "" ||
                client.phone == "" ||
                client.city == ""){
            Toast.makeText(this, "Please fill in all fields!", Toast.LENGTH_SHORT).show()
        } else{
            order.client = client
            val doc = db.collection("/organizations/test/users/${auth.currentUser!!.uid}/orders").document()
            order.id = doc.id
            var orderLinesToRemove = arrayListOf<OrderLine>()
            for (orderLine in order.orderLines!!) {
                if (orderLine.amount == 0) {
                    orderLinesToRemove.add(orderLine)
                }
            }
            order.orderLines!!.removeAll(orderLinesToRemove)
            doc.set(order)
            Toast.makeText(this, "Thank you for your order!", Toast.LENGTH_SHORT).show()
            homepage(view)
        }
    }

        fun homepage(view: View){
            val intent = Intent(this, CentralHub::class.java)
            startActivity(intent)
        }
    }

