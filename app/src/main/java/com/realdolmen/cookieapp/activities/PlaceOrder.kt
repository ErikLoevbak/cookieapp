package com.realdolmen.cookieapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.realdolmen.cookieapp.R
import com.realdolmen.cookieapp.domain.Order
import com.realdolmen.cookieapp.domain.OrderLine
import com.realdolmen.cookieapp.domain.Product
import kotlinx.android.synthetic.main.place_order.*

class PlaceOrder : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.place_order)
        auth = FirebaseAuth.getInstance()

        np_vanille.minValue = 0
        np_vanille.maxValue = 10
        np_choco.minValue = 0
        np_choco.maxValue = 10
        np_frangipane.minValue = 0
        np_frangipane.maxValue = 10
        np_carre.minValue = 0
        np_carre.maxValue = 10
        np_mix.minValue = 0
        np_mix.maxValue = 10
    }

    fun orderDetails(view : View){
        intent = Intent(this, ClientDetails::class.java)
        var orderLineVanille = OrderLine()
        var orderLineChoco = OrderLine()
        var orderLineFrangipane = OrderLine()
        var orderLineCarre = OrderLine()
        var orderLineMix = OrderLine()
        var order = Order()

        orderLineVanille.product = Product.PLAINWAFFLE
        orderLineChoco.product = Product.CHOCOLATEWAFFLE
        orderLineFrangipane.product = Product.FRANGIPANE
        orderLineCarre.product = Product.CARRE
        orderLineMix.product = Product.MIX

        orderLineVanille.amount = np_vanille.value
        orderLineChoco.amount = np_choco.value
        orderLineFrangipane.amount = np_frangipane.value
        orderLineCarre.amount = np_carre.value
        orderLineMix.amount = np_mix.value

        order.orderLines = mutableListOf()
        if(orderLineVanille.amount != 0) {
            order.orderLines!!.add(orderLineVanille)
        }
        if(orderLineChoco.amount != 0) {
            order.orderLines!!.add(orderLineChoco)
        }
        if(orderLineFrangipane.amount != 0) {
            order.orderLines!!.add(orderLineFrangipane)
        }
        if(orderLineCarre.amount != 0) {
            order.orderLines!!.add(orderLineCarre)
        }
        if(orderLineMix.amount != 0) {
            order.orderLines!!.add(orderLineMix)
        }
        if(order.orderLines!!.isEmpty()){
            Toast.makeText(this, "The order can't be empty!", Toast.LENGTH_SHORT).show()
        } else {
            intent.putExtra("order", order)
            startActivity(intent)
        }
    }



    fun homepage(view: View){
        val intent = Intent(this, CentralHub::class.java)
        startActivity(intent)
    }
}
