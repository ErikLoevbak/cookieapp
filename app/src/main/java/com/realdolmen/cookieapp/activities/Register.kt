package com.realdolmen.cookieapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.cookieapp.R
import com.realdolmen.cookieapp.domain.Member
import kotlinx.android.synthetic.main.login.*
import kotlinx.android.synthetic.main.register.*

class Register : AppCompatActivity() {

    private lateinit var auth : FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register)
        confirm_registration.setOnClickListener { register() }
        auth = FirebaseAuth.getInstance()
    }

    fun register(){
        if(password_register.text.toString() == confirm_password.text.toString()) {
            if (!email_register.text.isBlank() && !password_register.text.isBlank() && !confirm_password.text.isBlank()) {
                auth.createUserWithEmailAndPassword(email_register.text.toString(), password_register.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {

                            // Sign in success, update UI with the signed-in user's information
                            auth.currentUser!!.updateProfile(UserProfileChangeRequest.Builder().setDisplayName(display_name.text.toString()).build())
                            saveMember()
                            auth.signOut()
                            val intent = Intent(this, Login::class.java)
                            Toast.makeText(
                                baseContext, "register successful",
                                Toast.LENGTH_SHORT
                            ).show()
                            startActivity(intent)
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            } else {
                Toast.makeText(baseContext, "One of the fields is blank!", Toast.LENGTH_LONG).show()
            }
        } else {
            Toast.makeText(baseContext, "The passwords must match!", Toast.LENGTH_LONG).show()
        }
    }

    fun saveMember(){
        val db = FirebaseFirestore.getInstance()
        val member = Member()
        member.id = auth.currentUser!!.uid
        member.email = auth.currentUser!!.email
        member.displayName = display_name.text.toString()
        member.orders = mutableListOf()
        member.isAdmin = true
        db.collection("organizations/test/users").document(auth.currentUser!!.uid).set(member)
    }
}
