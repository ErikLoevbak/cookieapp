package com.realdolmen.cookieapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.cookieapp.R
import com.realdolmen.cookieapp.domain.Member
import kotlinx.android.synthetic.main.add_member.*
import kotlinx.android.synthetic.main.register.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class AddMember : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_member)
    }

    fun addMember(view : View) {
        val db = FirebaseFirestore.getInstance()
        val doc = db.collection("organizations/test/users").document()
        val member = Member()
        member.id = doc.id
        member.email = email_add_member.text.toString()
        member.displayName = full_name_add_member.text.toString()
        member.orders = mutableListOf()
        member.isAdmin = false
        if (member.email == "" ||
                member.displayName == ""){
            Toast.makeText(this, "Please fill in all fields", Toast.LENGTH_SHORT).show()
        } else {
            var pattern = Pattern.compile(".+@.+\\.[a-z]+")
            var  matcher = pattern.matcher(member.email)
            if (matcher.matches()) {
                db.collection("organizations/test/users").document(doc.id).set(member)
                homepage(view)
            } else{
                Toast.makeText(this, "Not a valid email address.", Toast.LENGTH_SHORT).show()
            }

        }

    }

    fun homepage(view: View) {
        val intent = Intent(this, CentralHub::class.java)
        startActivity(intent)
    }
}
