package com.realdolmen.cookieapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.cookieapp.MemberListAdapter
import com.realdolmen.cookieapp.R
import com.realdolmen.cookieapp.domain.Member
import com.realdolmen.cookieapp.domain.Order
import kotlinx.android.synthetic.main.view_members.*

class ViewMembers : AppCompatActivity() {
    private var layoutManager: RecyclerView.LayoutManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_members)
        loadList()

    }
    fun loadList(){
        var allMembers = arrayListOf<Member>()
        var db = FirebaseFirestore.getInstance()
        db.collection("organizations/test/users").get().addOnSuccessListener { result ->
            for(user in result){
                var member = Member()
                member.id = user.id
                member.email = user.get("email").toString()
                member.displayName = user.get("displayName").toString()
                member.isAdmin = user.get("admin").toString() == "true"
                allMembers.add(member)
            }
            loadingPanel.visibility = View.GONE
            if(allMembers.isEmpty()){
                no_items_to_display.text = "There are no members"
            } else {
                val adapter = MemberListAdapter(allMembers, this@ViewMembers)
                rec_members.adapter = adapter
                layoutManager = LinearLayoutManager(this@ViewMembers)
                rec_members.layoutManager = layoutManager
                adapter!!.notifyDataSetChanged()
            }
        }
    }
    fun homepage(view : View){
        val intent = Intent(this, CentralHub::class.java)
        startActivity(intent)
    }
}
