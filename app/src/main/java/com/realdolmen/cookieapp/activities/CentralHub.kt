package com.realdolmen.cookieapp.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.realdolmen.cookieapp.R



class CentralHub : AppCompatActivity() {
    lateinit var auth : FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.central_hub)

    }

    private var back_pressed: Long = 0
    private var toast: Toast? = null
    override fun onBackPressed() {


        if (back_pressed + 2000 > System.currentTimeMillis()) {

            // need to cancel the toast here
            toast!!.cancel()

            // code for exit
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)

        } else {
            // ask user to press back button one more time to close app
            toast = Toast.makeText(baseContext, "Press BACK again to exit", Toast.LENGTH_SHORT)
            toast!!.show()
        }
        back_pressed = System.currentTimeMillis()
    }

    fun placeOrder(view : View){
        val intent = Intent(this, PlaceOrder::class.java)
        startActivity(intent)
    }

    fun viewOrders(view : View){
        val intent = Intent(this, ViewOrders::class.java)
        auth = FirebaseAuth.getInstance()
        intent.putExtra("memberId", auth.currentUser!!.uid)
        startActivity(intent)
    }

    fun addMember(view : View){
        val intent = Intent(this, AddMember::class.java)
        startActivity(intent)
    }

    fun viewMembers(view : View){
        val intent = Intent(this, ViewMembers::class.java)
        startActivity(intent)
    }

    fun logout(view : View){
        val intent = Intent(this, Login::class.java)
        FirebaseAuth.getInstance().signOut()
        startActivity(intent)
    }

}
