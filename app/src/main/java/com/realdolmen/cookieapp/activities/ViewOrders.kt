package com.realdolmen.cookieapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.cookieapp.OrderListAdapter
import com.realdolmen.cookieapp.R
import com.realdolmen.cookieapp.domain.Order
import kotlinx.android.synthetic.main.view_orders.*
class ViewOrders : AppCompatActivity() {
    lateinit var memberId: String
    lateinit var auth : FirebaseAuth
    private var layoutManager: RecyclerView.LayoutManager?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_orders)
        memberId = intent.getStringExtra("memberId")
        auth = FirebaseAuth.getInstance()
        loadList()
    }

    private fun loadList() {
        val allOrders = arrayListOf<Order>()
        var db = FirebaseFirestore.getInstance()
        db.collection("organizations/test/users/$memberId/orders").get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    allOrders.add(document.toObject(Order::class.java))
                }
                loadingPanel.visibility = View.GONE
                if(allOrders.isEmpty()){
                    no_items_to_display.text = "No orders to display"
                } else {
                    val adapter = OrderListAdapter(allOrders, this@ViewOrders)
                    rec_orders.adapter = adapter
                    layoutManager = LinearLayoutManager(this@ViewOrders)
                    rec_orders.layoutManager = layoutManager
                    adapter!!.notifyDataSetChanged()
                }
            }
        if(memberId.equals(auth.currentUser!!.uid)){
            member_name.text = "Your orders"
        } else {
            db.document("organizations/test/users/$memberId").get().addOnSuccessListener { document ->
                member_name.text = "Orders by " + document["displayName"]
            }
        }
    }

    fun homepage(view : View){
        val intent = Intent(this, CentralHub::class.java)
        startActivity(intent)
    }
}
