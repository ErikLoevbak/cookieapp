package com.realdolmen.cookieapp.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.realdolmen.cookieapp.OrderLineListAdapter
import com.realdolmen.cookieapp.R
import com.realdolmen.cookieapp.domain.Client
import com.realdolmen.cookieapp.domain.Order
import com.realdolmen.cookieapp.domain.OrderLine
import com.realdolmen.cookieapp.domain.Product
import kotlinx.android.synthetic.main.order_details.*

class OrderDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.order_details)
        var order = intent.getSerializableExtra("order") as Order
        setContent(order)
        loadList(order.orderLines!! as ArrayList<OrderLine>)

    }

    private fun loadList(orderLines: ArrayList<OrderLine>){
        val adapter = OrderLineListAdapter(orderLines, this@OrderDetails)
        rec_orderlines.adapter = adapter
        var layoutManager = LinearLayoutManager(this@OrderDetails)
        rec_orderlines.layoutManager = layoutManager
        adapter!!.notifyDataSetChanged()
    }

    private fun setContent(order: Order){
        address_info.text = "${order.client!!.street} ${order.client!!.houseNumber}, ${order.client!!.postalCode} ${order.client!!.city}"
        phone_info.text = "${order.client!!.phone}"
        client_name.text = "Order for ${intent.getStringExtra("firstName")} ${intent.getStringExtra("lastName")}"
        products_bar.setOnClickListener{
            if(rec_orderlines.visibility == View.INVISIBLE) {
                rec_orderlines.visibility = View.VISIBLE
                show_products.rotation = 180f
                show_products2.rotation = 180f
                bottom_border.visibility = View.VISIBLE
            } else {
                rec_orderlines.visibility = View.INVISIBLE
                show_products.rotation = 0f
                show_products2.rotation = 0f
                bottom_border.visibility = View.INVISIBLE
            }
        }
    }

    fun homepage(view : View){
        val intent = Intent(this, CentralHub::class.java)
        startActivity(intent)
    }
}
