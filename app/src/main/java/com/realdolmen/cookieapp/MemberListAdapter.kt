package com.realdolmen.cookieapp

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.cookieapp.activities.ViewOrders
import com.realdolmen.cookieapp.domain.Member

class MemberListAdapter(private val list: ArrayList<Member>, private val context: Context) :
    RecyclerView.Adapter<MemberListAdapter.ViewHolder>() {


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(member: Member) {
            var db = FirebaseFirestore.getInstance()
            var memberName: TextView = itemView.findViewById(R.id.tv_member_name) as TextView
            var email: TextView = itemView.findViewById(R.id.email_view_members) as TextView
            var memberIcon: ImageView = itemView.findViewById(R.id.member_icon) as ImageView
            memberName.text = member.displayName
            email.text = member.email
            db.collection("organizations/test/users/${member.id}/orders").get().addOnSuccessListener { collection ->
                var orderCount = itemView.findViewById(R.id.total_orders) as TextView
                when {
                    collection.size() == 0 -> orderCount.text = "no orders"
                    collection.size() == 1 -> orderCount.text = collection.size().toString() + " order"
                    else -> orderCount.text = collection.size().toString() + " orders"
                }
                if (collection.size() != 0) {
                    orderCount.setOnClickListener {
                        val intent = Intent(context, ViewOrders::class.java)
                        intent.putExtra("memberId", member.id)
                        context.startActivity(intent)
                    }
                }

            }
            if (member.isAdmin == true) {
                memberIcon.setImageResource(R.drawable.admin)
            } else {
                memberIcon.setImageResource(R.drawable.user)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MemberListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cardview_member, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MemberListAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }
}