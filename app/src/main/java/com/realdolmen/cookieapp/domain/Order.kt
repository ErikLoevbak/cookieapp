package com.realdolmen.cookieapp.domain

import java.io.Serializable

class Order () : Serializable{
    var orderLines : MutableList<OrderLine>? = mutableListOf()
    var client : Client? = null
    var id : String? = null

    fun getFullPrice() : Float{
        var total : Float = 0.00f
        orderLines?.forEach{c -> total += c.getFullPrice()}
        return total
    }

    fun getItemAmount(): Int{
        var total : Int = 0
        orderLines?.forEach { c-> total += c.amount!! }
        return total
    }

    fun printFullOrder(){
        orderLines?.forEach {
            c -> println("${c.product!!.sName} : ${c.amount} stuks")
        }
        println("Totaalprijs: ${this.getFullPrice()}")
    }

}