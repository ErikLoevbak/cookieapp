package com.realdolmen.cookieapp.domain

abstract class Person (){
    open var firstName : String? = null
    open var lastName : String? = null

}