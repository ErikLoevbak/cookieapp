package com.realdolmen.cookieapp.domain

import java.io.Serializable

class Client() : Person(), Serializable{
    var id : Int? = null
    var street : String? = null
    var houseNumber : String? = null
    var city : String? = null
    var postalCode : String? = null
    var phone : String? = null
}