package com.realdolmen.cookieapp.domain

enum class Product (val sName:String, val price:Float, val weight:Float) {
    CHOCOLATEWAFFLE("Chocolate Waffle", 6f, 700f),
    PLAINWAFFLE("Plain Waffle", 6f, 700f),
    FRANGIPANE("Frangipane", 6f, 700f),
    CARRE("Carré Confituur", 7f, 600f),
    MIX("Mix", 8f, 1000f),
    TOTAL("Total", 8f, 1000f);
}