package com.realdolmen.cookieapp.domain

import java.io.Serializable

class OrderLine() : Serializable{
    var id: Int? = null
    var product : Product? = null
    var amount : Int? = null
    var order : Int? = null

    fun getFullPrice(): Float{
        return product!!.price*amount!!
    }
}