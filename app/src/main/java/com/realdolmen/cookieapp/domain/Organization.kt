package com.realdolmen.cookieapp.domain

class Organization(id : Int, name : String) {
    var id : Int = id
    var name : String = name
    var members : MutableList<Member> = mutableListOf()
}