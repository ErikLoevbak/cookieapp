package com.realdolmen.cookieapp.domain

class Member() : Person() {
    var id: String? = null
    var displayName: String? = null
    var email: String? = null
    var orders: MutableList<Order> = mutableListOf()
    var isAdmin: Boolean? = null

}