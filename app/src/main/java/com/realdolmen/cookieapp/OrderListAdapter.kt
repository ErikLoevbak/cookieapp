package com.realdolmen.cookieapp

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.realdolmen.cookieapp.activities.OrderDetails
import com.realdolmen.cookieapp.domain.Member
import com.realdolmen.cookieapp.domain.Order

class OrderListAdapter(private var list: ArrayList<Order>, private val context : Context) :
  RecyclerView.Adapter<OrderListAdapter.ViewHolder>()  {
    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun bindItem(order : Order) {
            var client: TextView = itemView.findViewById(R.id.tv_client) as TextView
            var price: TextView = itemView.findViewById(R.id.price) as TextView
            var vieweye: ImageView = itemView.findViewById(R.id.vieweye) as ImageView
            var itemamount: TextView = itemView.findViewById(R.id.tv_item_amount) as TextView
            price.text = "$${order.getFullPrice()}"
            client.text =   order.client!!.firstName + " " + order.client!!.lastName
            itemamount.text = order.getItemAmount().toString() + " items"
            vieweye.setOnClickListener {
                val intent = Intent(context, OrderDetails::class.java)
                intent.putExtra("firstName", order.client!!.firstName)
                intent.putExtra("lastName", order.client!!.lastName)
                intent.putExtra("order", order)
                context.startActivity(intent)
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): OrderListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cardview_order, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
        }

    override fun onBindViewHolder(holder: OrderListAdapter.ViewHolder, position: Int) {
            holder.bindItem(list[position])
        }
}